require 'rails_helper'

RSpec.describe Product, type: :model do

  context "save product" do
    it "should save a product" do
      @product = Product.new
      @product.name = "Pan con lomo"
      @product.description = "Pan con lomo y papas fritas, bien despachado tio."
      @product.category = "Sandwich"
      @product.stock = 50
      @product.price = 3.5
      @product.image = "pan_con_lomo.jpg"
      assert @product.save
    end
  end

end
